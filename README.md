# gitee_swift_client

### 练手用的 v3 api 

感谢以下开源

`Alamofire`
`SnapKit`
`MJRefresh`
`Kingfisher`
`HandyJSON`
`AwaitKit`
`PKHUD`
`UITableView+FDTemplateLayoutCell`
`MarkdownView`
`DZNEmptyDataSet`
`JXSegmentedView`
`BTNavigationDropdownMenu`

![效果图](https://gitee.com/wangxiaoshuai/gitee_swift_client/raw/master/GiteeClient/GiteeClient/screenshot/WX20201104-090421@2x.png)

![效果图](https://gitee.com/wangxiaoshuai/gitee_swift_client/raw/master/GiteeClient/GiteeClient/screenshot/WX20201104-090832@2x.png)

![效果图](https://gitee.com/wangxiaoshuai/gitee_swift_client/raw/master/GiteeClient/GiteeClient/screenshot/WX20201104-090917@2x.png)

![效果图](https://gitee.com/wangxiaoshuai/gitee_swift_client/raw/master/GiteeClient/GiteeClient/screenshot/WX20201104-090959@2x.png)
