//
//  UserDefaultTools.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/28.
//

import UIKit

/// 获取偏好设置
struct UserDefaultTools {

    static func setToken(token: String?){
        
        UserDefaults.standard.setValue(token, forKey: Common.userToken)
    }
    
    static func getToken()->String?{
        return UserDefaults.standard.string(forKey: Common.userToken)
    }
    
    static func setUser(user: UserModel?){
        
        UserDefaults.standard.setValue(user?.toJSONString(), forKey: Common.loginUser)
    }
    
    static func getUser()->UserModel?{
        
        let userJson = UserDefaults.standard.string(forKey: Common.loginUser)
        let user = UserModel.deserialize(from: userJson)
        return user
    }
    
    static func resetDefault(){
        setToken(token: nil)
        setUser(user: nil)
    }
}
