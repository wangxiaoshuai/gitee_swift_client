//
//  PopupViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import SnapKit
import UITableView_FDTemplateLayoutCell

/// 弹出列表选择控制器
class PopupViewController: UIViewController {

    private var titleStr:String!
    private var titleLabel:UILabel!
    private var contentTableView:UITableView!
    private var closeButton:UIButton!
    private var contentBGView:UIView!
    private var cellID = "listselectcellid"
    private var sources:[String]!
    
    init(title: String?, datas:[String]) {
        super.init(nibName: nil, bundle: nil)
        self.titleStr = title ?? "选择列表"
        self.sources = datas
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI(){
        
        self.view.frame = UIScreen.main.bounds
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        
        
        // 背景图
        contentBGView = UIView()
        if #available(iOS 13.0, *) {
            contentBGView.backgroundColor = Colors.backgroundColor
        }
        contentBGView.layer.cornerRadius = 8
        view.addSubview(contentBGView)
        contentBGView.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(40)
            make.bottom.right.equalToSuperview().offset(-40)
        }
        
        // 标题
        titleLabel = UILabel()
        titleLabel.font = .boldSystemFont(ofSize: 18)
        titleLabel.textColor = Colors.BLUE_COLOR
        titleLabel.textAlignment = .center
        titleLabel.text = titleStr
        contentBGView.addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
            make.height.equalTo(44)
        }
        
        // 关闭按钮
        closeButton = UIButton(type: .custom)
        closeButton.setTitle("取消", for: .normal)
        closeButton.setTitleColor(Colors.BLUE_COLOR, for: .normal)
        closeButton.setTitleColor(.darkGray, for: .highlighted)
        closeButton.addTarget(self, action: #selector(closeButtonClick), for: .touchUpInside)
        contentBGView.addSubview(closeButton)
        
        closeButton.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-5)
        }
        
        // 列表
        contentTableView = UITableView()
        if #available(iOS 13.0, *) {
            contentTableView.backgroundColor = Colors.backgroundColor
        }
        contentTableView.register(PopupTableViewCell.self, forCellReuseIdentifier: cellID)
        contentTableView.separatorStyle = .none
        contentBGView.addSubview(contentTableView)
        
        contentTableView.delegate = self
        contentTableView.dataSource = self
                
        contentTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(64)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
      
        
    }
}

extension PopupViewController {
    
    @objc func closeButtonClick(){
        
//        self.dismiss(animated: true, completion: nil)
        self.view.removeFromSuperview()
    }
    
    func show(){
        
//        let navc = BaseNavigationController(rootViewController: self)
        
//        self.modalPresentationStyle = .fullScreen
//        if #available(iOS 13.0, *) {
//            self.isModalInPresentation = true
//        }
        
        UIApplication.shared.keyWindow?.rootViewController?.addChild(self)
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self.view)
    }
    
}


extension PopupViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! PopupTableViewCell
        cell.titleLabel.text = sources[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: cellID) { (cell) in
            
            (cell as! PopupTableViewCell).titleLabel.text = self.sources[indexPath.row]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
