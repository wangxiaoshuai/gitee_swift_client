//
//  NoteModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/26.
//

import UIKit
import HandyJSON

struct NoteModel: HandyJSON {
    
    // id
    var id:Int!
    // body
    var body:String!
    // attachment
    var attachment:String!
    // author
    var author:UserModel!
    // created_at
    var created_at:String!

}
