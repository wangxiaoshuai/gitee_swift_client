//
//  CommitDetialTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

import UIKit

class CommitDetialTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var createdDateLabel: UILabel!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    var model:CommitModel?{
        didSet{
            guard let m = model else {
                return
            }
            
            let url = m.author?.portrait_url ?? m.author?.new_portrait
            
            if let icon = URL(string: url ?? "") {
                userImageView.kf.setImage(with: icon, placeholder: UIImage(named: "image_loading"), options: .none, progressBlock: nil, completionHandler: nil)
            }
            
            userNameLabel.text = m.author?.name ?? m.author_name
            createdDateLabel.text = "创建于 \(m.created_at.intervalSinceNow())"
            self.contentLabel.text = m.title
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 13.0, *) {
            self.contentView.backgroundColor = Colors.backgroundColor
            contentLabel.textColor = Colors.labelColor
        } else {
            self.contentView.backgroundColor = .white
        }
        userNameLabel.textColor = Colors.BLUE_COLOR
        
        var bounds = self.userImageView.bounds
        bounds.size.height -= 4
        bounds.size.width -= 4
        bounds.origin.x += 1
        bounds.origin.y += 1
        // Add rounded corners
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds//self.AutorImageView.bounds
        maskLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 20).cgPath
        self.userImageView.layer.mask = maskLayer
        
        // Add border
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.lightGray.cgColor
        borderLayer.lineWidth = 0.5
        borderLayer.frame = bounds
        self.userImageView.layer.addSublayer(borderLayer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
