//
//  MyWebView.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import WebKit

class MyWebView: WKWebView {
    
    required init?(coder: NSCoder) {
        let frame = UIScreen.main.bounds
        let config = WKWebViewConfiguration()
        super.init(frame: frame, configuration: config)
        self.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
   
}
