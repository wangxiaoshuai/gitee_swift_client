//
//  ProjectDeitalHeaderTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/20.
//

import UIKit

protocol ProjectDeitalHeaderTableViewCellDelegate {
    func starButtonClick()
    func watchButtonClik()
}

/// 项目详情界面 顶部用于展示项目简介  fork star等信息的cell
class ProjectDeitalHeaderTableViewCell: UITableViewCell {

    var delegate:ProjectDeitalHeaderTableViewCellDelegate?
    
    // 项目描述
    @IBOutlet weak var projectDescLabel: UILabel!
    // 创建时间
    @IBOutlet weak var createTimeButton: UIButton!
    // 项目权限状态
    @IBOutlet weak var publicButton: UIButton!
    // fork数量
    @IBOutlet weak var forkButton: UIButton!
    // 语言
    @IBOutlet weak var languageButton: UIButton!
    // watch数量 需要注意当点击的时候要变更 unwatch
    @IBOutlet weak var watchButton: UIButton!
    // star数量 点击要变化显示unstar
    @IBOutlet weak var starButton: UIButton!
    
    var model:ProjectMdoel?{
        didSet{
            guard let m = model else { return }
            setDataToUI(model: m)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if #available(iOS 13.0, *) {
            self.contentView.backgroundColor = Colors.backgroundColor
            self.projectDescLabel.textColor = Colors.labelColor
        } else {
            self.contentView.backgroundColor = .white
            self.projectDescLabel.textColor = .white
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    // 将数据设置到界面上
    func setDataToUI(model: ProjectMdoel){
        projectDescLabel.text = model.description
        createTimeButton.setTitle(model.created_at.intervalSinceNow(), for: .normal)
        publicButton.setTitle(model.isPublic ? "Public" : "Private", for: .normal)
        forkButton.setTitle(model.forks_count.description, for: .normal)
        languageButton.setTitle(model.language, for: .normal)
        
        let starStr = model.stared ? "unstar \(model.stars_count.description)" : "star \(model.stars_count.description)"
        starButton.setTitle(starStr, for: .normal)
        
        let watchStr = model.watched ? "unwatch \(model.watches_count.description)" : "watch \(model.watches_count.description)"
        watchButton.setTitle(watchStr, for: .normal)
        
    }
    
    // watch按钮点击
    @IBAction func watchButtonClick(_ sender: Any) {
        delegate?.watchButtonClik()
    }
    
    // star按钮点击
    @IBAction func starButtonClick(_ sender: UIButton) {
        delegate?.starButtonClick()
    }
    
}
