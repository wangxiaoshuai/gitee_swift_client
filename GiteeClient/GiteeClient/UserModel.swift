//
//  UserModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/15.
//

import UIKit
import HandyJSON

struct UserModel:HandyJSON  {
    
    var id:Int!
    var username:String!
    var email:String?
    var state:String!
    var created_at:String!
    var portrait_url:String?
    var name:String!
    var new_portrait:String?
    
    // bio
    var bio:String!
    // weibo
    var weibo:String!
    // blog
    var blog:String!
    // theme_id
    var theme_id:Int!
    // private_token
    var private_token:String!
    // is_admin
    var is_admin:Bool!
    // can_create_group
    var can_create_group:Bool!
    // can_create_project
    var can_create_project:Bool!
    // can_create_team
    var can_create_team:Bool!
    // follow
    var follow:[String:Any]!
}
