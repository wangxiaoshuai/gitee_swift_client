//
//  DiffModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

import UIKit
import HandyJSON

struct DiffModel: HandyJSON {

    // diff
    var diff:String!
    // new_path
    var new_path:String!
    // old_path
    var old_path:String!
    // a_mode
    var a_mode:String!
    // b_mode
    var b_mode:String!
    // new_file
    var new_file:String!
    // renamed_file
    var renamed_file:Bool!
    // deleted_file
    var deleted_file:Bool!
    // type
    var type:String!
}
