//
//  CommentModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

import UIKit
import HandyJSON

/// 提交中的评论模型
struct CommentModel: HandyJSON {

    var id:Int!
    var created_at:String!
    var project_id:Int!
    var commit_id:Int!
    var author:UserModel!
    var note:String! //查询时候显示
    var private_token:String?
    
    var body:String?// 发表评论的时候 评论内容存放在此字段
    var source:String! // "api"
    var target:IssueModel?
    
}

/*
 "id": 3528469,
     "body": "\u003Cp\u003E说的是个啥\u003C/p\u003E",
     "author": {
         "id": 137624,
         "username": "wangxiaoshuai",
         "email": "505040542@qq.com",
         "state": "active",
         "created_at": "2014-12-05T18:11:18+08:00",
         "portrait_url": "https://portrait.gitee.com/uploads/avatars/user/45/137624_wangxiaoshuai_1578919244.jpg",
         "name": "就爱数硬币",
         "new_portrait": "https://portrait.gitee.com/uploads/avatars/user/45/137624_wangxiaoshuai_1578919244.jpg"
     },
     "source": "api",
     "created_at": "2020-10-30T14:13:58+08:00",
     "target": {
         "issue": {
             "id": 3514380,
             "iid": 5,
             "title": "不明觉厉"
         },
         "pull_request": null
     }
 }
 */
