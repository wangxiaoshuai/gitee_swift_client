//
//  CommitBodyTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

import UIKit

class CommitBodyTableViewCell: UITableViewCell {

    @IBOutlet weak var commitTitleLabel: UILabel!
    
    @IBOutlet weak var commitSubTitleLabel: UILabel!
    
    
    var model:DiffModel?{
        didSet{
            guard let m = model else {
                return
            }
            
            if m.new_path.contains("/") {
                let strs = m.new_path.split(separator: "/")
                let lastStr = strs.last
                
                let nss = NSString(string: m.new_path!)
                
                self.commitTitleLabel.text = lastStr!.description
                self.commitSubTitleLabel.text = nss.substring(to: m.new_path!.count-lastStr!.count-1)
                
            }else {
                self.commitTitleLabel.text = m.new_path!.description
                self.commitSubTitleLabel.text = m.new_path!.description
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 13.0, *) {
            self.contentView.backgroundColor = Colors.backgroundColor
            commitTitleLabel.textColor = Colors.labelColor
        } else {
            self.contentView.backgroundColor = .white
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
