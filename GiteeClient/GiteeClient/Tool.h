//
//  Tool.h
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Tool : NSObject

+ (NSString *)flattenHTML:(NSString *)html;

@end

NS_ASSUME_NONNULL_END
