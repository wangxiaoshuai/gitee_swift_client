//
//  URLs.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/14.
//

import UIKit

/// 定义访问请求的url
struct URLs {

   
    static let REDIRURL = "https://gitee.com/wangxiaoshu"
    static let CLIENT_ID = "your CLIENT_ID "
    static let CLIENT_SECRET = "your CLIENT_SECRET"
    
    /// https://gitee.com/
    static let HTTPSFIX = "https://gitee.com/"
    static let PROJECTS = "projects/"
    static let USER = "user/"
    static let EVENTS = "events/"
    static let LANGUAGE = "languages/"
     /// readme项目
    static let READMEURL = "readme/"
    /// v3 api
    static let V3URL = "https://gitee.com/api/v3/"
    /// 推荐项目
    static let FEATURED_URL = V3URL + "projects/featured"
    /// 热门项目
    static let POPULAR_URL = V3URL + "projects/popular"
    /// 最近更新项目
    static let LAST_URL = V3URL + "projects/latest"
    /// 项目明细
    static let DETIAL_URL = "https://gitee.com/api/v3/projects/"
    /// 搜索项目
    static let SEAECHURL = "https://gitee.com/api/v3/projects/search/"
    
    /// v3 的授权地址
    static let AOUTHURL = "https://gitee.com/api/v3/session"

   
    
}
