//
//  WatchStarResultModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import HandyJSON

/// watch  star 点击按钮结果模型
struct WatchStarResultModel: HandyJSON {

    var count:Int!
}
