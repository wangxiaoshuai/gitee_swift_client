//
//  EventMdoel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/11/2.
//

import UIKit
import HandyJSON

/*
 public final static byte EVENT_TYPE_CREATED   = 0x1;// 创建了issue
     public final static byte EVENT_TYPE_UPDATED   = 0x2;// 更新项目
     public final static byte EVENT_TYPE_CLOSED    = 0x3;// 关闭项目
       public final static byte EVENT_TYPE_REOPENED  = 0x4;// 重新打开了项目
       public final static byte EVENT_TYPE_PUSHED    = 0x5;// push
       public final static byte EVENT_TYPE_COMMENTED = 0x6;// 评论
       public final static byte EVENT_TYPE_MERGED    = 0x7;// 合并
       public final static byte EVENT_TYPE_JOINED    = 0x8; //# User joined project
       public final static byte EVENT_TYPE_LEFT      = 0x9; //# User left project
       public final static byte EVENT_TYPE_FORKED    = 0xb;// fork了项目
 */
enum actionEnum:Int {
   case CREATED = 0x1, UPDATED = 0x2, CLOSED = 0x3, REOPENED = 0x4, PUSHED = 0x5,
        COMMENTED = 0x6, MERGED = 0x7, JOINED = 0x8, LEFT = 0x9, FORKED = 0xb, DefaultUpdate = 1000
}

struct EventMdoel: HandyJSON {
    
    // id
    var eventID:Int!
    // target_type
    var target_type:String!
    // target_id
    var target_id:String!
    // title
    var title:String!
    // data
    var data:[String:Any]!

    // project_id
    var project_id:Int!
    // created_at
    var created_at:String!
    // updated_at
    var updated_at:String!
    // action
    var action:Int!
    // author_id
    var author_id:Int!
    // public
    var isPublic:Bool!
    // project
    var project:ProjectMdoel!
    // user
    var author:UserModel!
    // events
    var events:EventDataModel!
    
    init() {}
    
    mutating func mapping(mapper: HelpingMapper) {
        
        mapper <<<
            self.eventID <-- "id"
        mapper <<<
            self.author <-- "author"
        mapper <<<
            self.isPublic <-- "public"
            
    }
    
    /// 获取event的描述
    func getEventDescriptionForEvent()-> NSMutableAttributedString{
        
        // 用户名
        let autorFont = UIFont.systemFont(ofSize: 15)
        let autorColor = UIColor(red: 14/255.0, green: 89/255.0, blue: 134/255.0, alpha: 1)
        let authorStrAttributes = [NSAttributedString.Key.font: autorFont, NSAttributedString.Key.foregroundColor: autorColor]
        
        let eventDescription = NSMutableAttributedString(string: self.author?.name ?? "", attributes: authorStrAttributes)
        
        // 动作action
        let actionColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
        let actionAttributes = [NSAttributedString.Key.font: autorFont, NSAttributedString.Key.foregroundColor: actionColor]
        
        // 项目
        let projectColor = UIColor(red: 13/255.0, green: 109/255.0, blue: 168/255.0, alpha: 1)
        let projectStrAttributes = [NSAttributedString.Key.font: autorFont, NSAttributedString.Key.foregroundColor: projectColor]
        let projectName = self.project.owner.name + "/" + self.project.name
        let project = NSMutableAttributedString(string: projectName, attributes: projectStrAttributes)
       
        // 根据action 来区分是更新还是评论等
        let type:actionEnum = actionEnum(rawValue: self.action!) ?? .DefaultUpdate
        
        var action = NSMutableAttributedString()
        switch type {
        case .CREATED:
            action = NSMutableAttributedString(string: " 在项目  创建了 ", attributes: actionAttributes)
            action.insert(project, at: 5)
            action.append(NSMutableAttributedString(string: self.eventTitle(), attributes: projectStrAttributes))
        case .UPDATED:
            action = NSMutableAttributedString(string: " 更新了项目 ", attributes: actionAttributes)
            action.append(NSMutableAttributedString(string: project.string, attributes: projectStrAttributes))
        case .CLOSED:
            action = NSMutableAttributedString(string: " 关闭了项目  的 ", attributes: actionAttributes)
            action.insert(project, at: 7)
            action.append(NSMutableAttributedString(string: self.eventTitle(), attributes: projectStrAttributes))
        case .REOPENED:
            action = NSMutableAttributedString(string: " 重新打开了项目  的 ", attributes: actionAttributes)
            action.insert(project, at: 9)
            action.append(NSMutableAttributedString(string: self.eventTitle(), attributes: projectStrAttributes))
        case .PUSHED:
            action = NSMutableAttributedString(string: " 推送到了项目  的分支", attributes: actionAttributes)
            action.insert(project, at: 8)
            let name = (self.data["ref"] as! NSString).lastPathComponent
            action.insert(NSMutableAttributedString(string: name, attributes: projectStrAttributes), at: action.length-2)
        case .COMMENTED:
            action = NSMutableAttributedString(string: " 评论了项目  的 ", attributes: actionAttributes)
            action.insert(project, at: 7)
            action.append(NSMutableAttributedString(string: "", attributes: projectStrAttributes))
        case .MERGED:
            action = NSMutableAttributedString(string: " 接受了项目  的 ", attributes: actionAttributes)
            action.insert(project, at: 7)
            action.append(NSMutableAttributedString(string: "", attributes: projectStrAttributes))
        case .JOINED:
            action = NSMutableAttributedString(string: " 加入了项目 ", attributes: actionAttributes)
            action.append(project)
        case .LEFT:
            action = NSMutableAttributedString(string: " 离开了项目 ", attributes: actionAttributes)
            action.append(project)
        case .FORKED:
            action = NSMutableAttributedString(string: " Fork了项目 ", attributes: actionAttributes)
            action.append(project)
        default:
            action = NSMutableAttributedString(string: " 更新了动态 ", attributes: actionAttributes)
        }
        
        eventDescription.append(action)
        
        return eventDescription
    }
    
    /// 获取event 的title
    func eventTitle()->String{
        
        var title = ""
        
        if let preq = events.pullrequest {
            if let iid = preq.mergeRequestIid {
                title = "Pull Request #" + iid.description
                return title
            }
        }
        if let preq = events.issue {
            if let iid = preq.iid {
                title = "Issue #" + iid.description
                return title
            }
        }
        return title
    }
    
    /// 获取event 的内容
    func eventContent()->String{
        var title = ""
        if let preq = events.pullrequest {
            return preq.title
        }
        if let preq = events.issue {
            return preq.title
        }
        
        return title
    }
    
}

struct EventDataModel: HandyJSON {
    
    
//    var totalCommitCount:Int!
//    var ref:String!
//    var dataCommits:[String]!
    
    var issue:IssueModel?
    var pullrequest:RequestModel?
    var note:CommentModel?
    
    init(){}
    mutating func mapping(mapper: HelpingMapper) {
        
        mapper <<<
            self.pullrequest <-- "pull_request"
    }
}
