//
//  Colors.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/22.
//

import UIKit

/// 定义app中常用的颜色
@objcMembers class Colors:NSObject {

    static let BLUE_COLOR = UIColor.init(red: 70/255, green: 130/255, blue: 180/255, alpha: 1)
    
    @available(iOS 13.0, *)
    static let backgroundColor = UIColor { (traict) -> UIColor in
        if traict.userInterfaceStyle == .dark {
            // 深灰色
            return UIColor(red: 25/255, green: 25/255, blue: 25/255, alpha: 1)
        }else {
            return .white
        }
    }
    
    @available(iOS 13.0, *)
    static let navigationColor = UIColor { (traict) -> UIColor in
        if traict.userInterfaceStyle == .dark {
            // 深灰色
            return UIColor(red: 17/255, green: 17/255, blue: 17/255, alpha: 1)
        }else {
            return .white
        }
    }
    
    @available(iOS 13.0, *)
    static let labelColor = UIColor { (traict) -> UIColor in
        if traict.userInterfaceStyle == .dark {
            // 深灰色
            return UIColor(red: 197/255, green: 197/255, blue: 198/255, alpha: 1)
        }else {
            return .black
        }
    }
    
    
    @available(iOS 13.0, *)
    static let subBackgroundColor = UIColor { (traict) -> UIColor in
        if traict.userInterfaceStyle == .dark {
            // 深灰色
            return UIColor(red: 197/255, green: 197/255, blue: 198/255, alpha: 1)
        }else {
            return .systemGray6
        }
    }
    
    @available(iOS 13.0, *)
    static let mjrefreshTextColor = UIColor { (traict) -> UIColor in
        if traict.userInterfaceStyle == .dark {
            // 深灰色
            return UIColor(red: 197/255, green: 197/255, blue: 198/255, alpha: 1)
        }else {
            return .lightGray
        }
    }
    
}
