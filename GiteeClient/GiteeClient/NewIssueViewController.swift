//
//  NewIssueViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import SnapKit
import PKHUD


protocol NewIssueViewControllerDelegate {
    func didCreateIssue()
}

class NewIssueViewController: BaseViewController {

    var delegate:NewIssueViewControllerDelegate?
    
    var titleLabel:UILabel!
    var titleTextField:UITextField!
    var contentLabel:UILabel!
    var contentTextArea:UITextView!
    var doneButton:UIButton!
    
    var projectM:ProjectMdoel?
    
    init(project:ProjectMdoel) {
        super.init(nibName: nil, bundle: nil)
        self.projectM = project
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
}

// MARK: - 设置界面
extension NewIssueViewController {
    
    func setupUI() {
    
        if #available(iOS 13.0, *) {
            view.backgroundColor = Colors.backgroundColor
        }
        
        title = "新建issue"
        titleLabel = UILabel()
        titleLabel.textColor = Colors.BLUE_COLOR
       
        view.addSubview(titleLabel)
        titleLabel.text = "Issue标题"
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(20)
        }
        
        titleTextField = UITextField()
        view.addSubview(titleTextField)
        titleTextField.placeholder = "issue 标题"
        titleTextField.layer.borderWidth = 0.5
        titleTextField.layer.borderColor = Colors.BLUE_COLOR.cgColor
        
        titleTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(44)
        }
        
        
        contentLabel = UILabel()
        contentLabel.textColor = Colors.BLUE_COLOR
        contentLabel.text = "Issue内容"
        view.addSubview(contentLabel)
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(titleTextField.snp.bottom).offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        contentTextArea = UITextView()
        view.addSubview(contentTextArea)
        contentTextArea.font = .systemFont(ofSize: 16)
        contentTextArea.layer.borderWidth = 0.5
        contentTextArea.layer.borderColor = Colors.BLUE_COLOR.cgColor
        if #available(iOS 13.0, *) {
            contentTextArea.backgroundColor = Colors.backgroundColor
        }
        contentTextArea.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(contentLabel.snp.bottom).offset(5)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(200)
        }
        
        doneButton = UIButton(type: .custom)
        doneButton.setTitle("提交", for: .normal)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.backgroundColor = Colors.BLUE_COLOR
        view.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(doneButtonClick), for: .touchUpInside)
        doneButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(contentTextArea.snp.bottom).offset(30)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(44)
        }
        
    }
}

extension NewIssueViewController {
    
    @objc func doneButtonClick(){
        
        if File.isNUllOrEmpty(string: titleTextField.text) {
            HUD.flash(.label("issue 标题不能为空"), delay: 1.0)
            return
        }
        
        HUD.show(.progress)
        var issue = IssueModel()
        issue.id = self.projectM!.id
        issue.description = contentTextArea.text ?? ""
        issue.title = titleTextField.text!
        issue.private_token = UserDefaultTools.getToken()!
        
        let url = URLs.V3URL + URLs.PROJECTS + self.projectM!.id.description + "/issues"
        // 异步加载
        DispatchQueue.global().async {
        
            let result:Result<IssueModel> = NetWorkTool.shared.getRequest(url: url, method: .post, parameters: issue.toJSON())
            
            DispatchQueue.main.async {
                
                HUD.hide()
                if result.code == 0 {
                    guard let _ = result.data else {
                        HUD.flash(.label("创建失败"), delay: 1.0)
                        return
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.didCreateIssue()
                    
                }else {
                    HUD.flash(.label(result.message), delay: 2.0)
                }
            }
        }
    }
}
