//
//  IssueModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/23.
//

import UIKit
import HandyJSON

/// 问题模型
struct IssueModel: HandyJSON {
    
    // issue_id / id
    var id:Int!
    // iid
    var iid:Int!
    // id / project_id
    var project_id:Int!
    // title
    var title:String!
    // description
    var description:String!
    // labels
    var labels:[String]?
    // milestone
    var milestone:MilestoneModel!
    // assignee
    var assignee:UserModel!
    // author
    var author:UserModel!
    // author_id
    var author_id:Int!
    // state
    var state:String!
    // updated_at
    var updated_at:String!
    // created_at
    var created_at:String!

    var private_token:String?//    Eeu8RUV6SgCacx3U45UP
}
