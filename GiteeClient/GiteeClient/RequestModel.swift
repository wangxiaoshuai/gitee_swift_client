//
//  RequestModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/11/2.
//

import UIKit
import HandyJSON

struct RequestModel: HandyJSON {

    /// id
    var mergeRequestId:Int!
    /// iid
    var mergeRequestIid:Int!
    /// title
    var title:String!
    /// state
    var state:String!
    // author
    var author:UserModel!
    // assignee
    var assignee:UserModel!
    // target_branch
    var targetBranch:String!
    // source_branch
    var sourceBranch:String!
    // project_id
    var projectId:Int!
    // upvotes
    var upvotes:Int!
    // downvotes
    var downvotes:Int!
    
    init() {}
    
    mutating func mapping(mapper: HelpingMapper) {
        
        mapper <<<
            self.mergeRequestId <-- "id"
        mapper <<<
            self.mergeRequestIid <-- "iid"
        mapper <<<
            self.targetBranch <-- "target_branch"
        mapper <<<
            self.sourceBranch <-- "source_branch"
        mapper <<<
            self.projectId <-- "project_id"
            
    }
}
