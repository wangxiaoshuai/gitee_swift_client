//
//  CodeFolderModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/22.
//

import UIKit
import HandyJSON

struct CodeFolderModel: HandyJSON {

    var name:String!
    var type:String!
    var id:String!
    var mode:String!
}
