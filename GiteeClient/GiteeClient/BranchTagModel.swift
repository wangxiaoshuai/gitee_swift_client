//
//  BranchTagModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import HandyJSON

/// 分支模型  branch  tag
struct BranchTagModel: HandyJSON {
    
    var name:String!
    var commit:CommitModel!
    var defaultStr:String!
    var protected:Bool!
    
    init() {}
    
    mutating func mapping(mapper: HelpingMapper) {
        mapper <<<
            self.defaultStr <-- "default"
       
    }

}
