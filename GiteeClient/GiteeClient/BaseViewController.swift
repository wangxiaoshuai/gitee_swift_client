//
//  BaseViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/23.
//

import UIKit
import MJRefresh
import DZNEmptyDataSet

/// 跟控制器 定义通用属性
class BaseViewController: UIViewController ,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{

    // 这个闭包是分野控制器使用的
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            view.backgroundColor = Colors.navigationColor
        } else {
            view.backgroundColor = .white
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "page_icon_empty")!
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "暂无数据!")
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        true
    }
    
    
}


extension MJRefreshNormalHeader {
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        if #available(iOS 13.0, *) {
            self.stateLabel.textColor = Colors.mjrefreshTextColor
            
        } else {
           
        }
    }
}

extension MJRefreshBackNormalFooter {
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        if #available(iOS 13.0, *) {
            self.stateLabel.textColor = Colors.mjrefreshTextColor
        } else {
           
        }
    }
}
