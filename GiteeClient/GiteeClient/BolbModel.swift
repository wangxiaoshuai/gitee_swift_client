//
//  BolbModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/23.
//

import UIKit
import HandyJSON


/// 文件模型
struct BolbModel: HandyJSON {

    var file_name:String!
    var file_path:String!
    var size:Int!
    var encoding:String!
    var content:String!
    var ref:String!
    var bolb_id:String!
    var commit_id:String!
}
