//
//  PopupTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import SnapKit


class PopupTableViewCell: UITableViewCell {

    var titleLabel:UILabel!
    var lineView:UIView!

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI(){
        titleLabel = UILabel()
        titleLabel.textAlignment = .center
        if #available(iOS 13.0, *) {
            titleLabel.textColor = Colors.labelColor
        }else {
            titleLabel.textColor = .darkGray
        }
        contentView.addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalToSuperview()
        }
        
        lineView = UIView()
        lineView.backgroundColor = Colors.BLUE_COLOR
        contentView.addSubview(lineView)
        
        lineView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.height.equalTo(0.5)
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
