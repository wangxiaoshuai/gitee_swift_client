//
//  IssueTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/23.
//

import UIKit
import Kingfisher

class IssueTableViewCell: UITableViewCell {

    
    @IBOutlet weak var issueImageView: UIImageView!
    
    @IBOutlet weak var bySomeOneLabel: UILabel!
    
    @IBOutlet weak var issueDescLabel: UILabel!
    
    @IBOutlet weak var createTimeButton: UIButton!
    
    var model:IssueModel?{
        didSet{
            guard let m = model else {
                return
            }
            issueImageView.kf.setImage(with: URL(string: m.author.portrait_url!), placeholder: UIImage(named: "image_loading"), options: nil, progressBlock: nil, completionHandler: nil)
            bySomeOneLabel.text = "#\(m.iid!.description)  by \(m.author.name.description)"
            issueDescLabel.text = m.title
            createTimeButton.setTitle(m.created_at.intervalSinceNow(), for: .normal)
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if #available(iOS 13.0, *) {
            self.contentView.backgroundColor = Colors.backgroundColor
            issueDescLabel.textColor = Colors.labelColor
        } else {
            self.contentView.backgroundColor = .white
            issueDescLabel.textColor = .black
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
