//
//  NewCommitCommentViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import PKHUD
import SnapKit


protocol NewCommitCommentViewControllerDelegate {
    func didCreateCommitComment()
}

/// 新建提交 评论控制器
class NewCommitCommentViewController: BaseViewController {

    var delegate:NewCommitCommentViewControllerDelegate?
    
    var contentLabel:UILabel!
    var contentTextArea:UITextView!
    var doneButton:UIButton!

    var commitM:CommitModel?
    var projectM:ProjectMdoel?
    
    init(commit:CommitModel, project:ProjectMdoel) {
        super.init(nibName: nil, bundle: nil)
        self.projectM = project
        self.commitM = commit
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
}

// MARK: - 设置界面
extension NewCommitCommentViewController {
    
    func setupUI() {
    
        title = "发表评论"
        
        if #available(iOS 13.0, *) {
            view.backgroundColor = Colors.backgroundColor
        }
        
        contentLabel = UILabel()
        contentLabel.textColor = Colors.BLUE_COLOR
        contentLabel.text = "评论内容"
        view.addSubview(contentLabel)
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        contentTextArea = UITextView()
        view.addSubview(contentTextArea)
        contentTextArea.font = .systemFont(ofSize: 16)
        contentTextArea.layer.borderWidth = 0.5
        contentTextArea.layer.borderColor = Colors.BLUE_COLOR.cgColor
        if #available(iOS 13.0, *) {
            contentTextArea.backgroundColor = Colors.backgroundColor
        }
        contentTextArea.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(contentLabel.snp.bottom).offset(5)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(200)
        }
        
        doneButton = UIButton(type: .custom)
        doneButton.setTitle("提交", for: .normal)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.backgroundColor = Colors.BLUE_COLOR
        view.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(doneButtonClick), for: .touchUpInside)
        doneButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(contentTextArea.snp.bottom).offset(30)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(44)
        }
        
    }
}

extension NewCommitCommentViewController {
    
    @objc func doneButtonClick(){
        
        if File.isNUllOrEmpty(string: contentTextArea.text) {
            HUD.flash(.label("评论内容不能为空"), delay: 1.0)
            return
        }
        
        HUD.show(.progress)
        var comment = CommentModel()
        comment.private_token = UserDefaultTools.getToken()!
        comment.note = contentTextArea.text
        
        let url = URLs.V3URL + URLs.PROJECTS + self.projectM!.id.description + "/repository/commits/\( self.commitM!.id!.description)/comment"
        
         // 异步加载
        DispatchQueue.global().async {
        
            let result:Result<CommentModel> = NetWorkTool.shared.getRequest(url: url, method: .post, parameters: comment.toJSON())
            
            DispatchQueue.main.async {
                
                HUD.hide()
                if result.code == 0 {
                    guard let _ = result.data else {
                        HUD.flash(.label("创建失败"), delay: 1.0)
                        return
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.didCreateCommitComment()
                    
                }else {
                    HUD.flash(.label(result.message), delay: 2.0)
                }
            }
        }
    }
}
