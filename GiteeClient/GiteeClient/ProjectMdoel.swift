//
//  ProjectMdoel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/15.
//

import UIKit
import HandyJSON


/// 返回项目数组模型
struct ProjectArrayModel: HandyJSON {
    
    var items:[ProjectMdoel]?
    init() {
        
    }
}


/// 项目模型
struct ProjectMdoel: HandyJSON {

    var id:Int!
    var name:String!
    var default_branch:String!
    var description:String!
    var owner:UserModel!
    var isPublic:Bool!
    var path:String!
    var path_with_namespace:String!
    var name_with_namespace:String!
    var issues_enabled:Bool!
    var pull_requests_enabled:Bool!
    var wiki_enabled:Bool!
    var created_at:String!
    var namespace:NameSpaceModel!
    var last_push_at:String?
    var parent_id:Int!
    var isFork:Bool!
    var forks_count:Int!
    var stars_count:Int!
    var watches_count:Int!
    var language:String!
    var pass:String?
    var stared:Bool = false
    var watched:Bool = false
    var relation:String?
    var recomm:Bool!
    var parent_path_with_namespace:String?
   
    init() {}
    
    mutating func mapping(mapper: HelpingMapper) {
        mapper <<<
            self.isFork <-- "fork?"
        mapper <<<
            self.isPublic <-- "public"
    }
}
