//
//  UserHeaderView.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/29.
//

import UIKit
import Kingfisher


protocol userNameClickDelegate {
    func nameClick()
}

/// 个人主页 顶部视图
class UserHeaderView: UIView {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var watchedLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var staredLabel: UILabel!
    @IBOutlet weak var followsLabel: UILabel!
    @IBOutlet weak var userDescLabel: UILabel!
    @IBOutlet weak var joinDateLabel: UILabel!
    
    var imageViewFrame: CGRect!
    
    var delegate:userNameClickDelegate?
    
    var model:UserModel?{
        didSet{
            guard let user = model else {
                self.resetUI()
                return
            }
            setupUI(user: user)
        }
    }
    
    @IBAction func loginButtonClick(_ sender: Any) {
        self.delegate?.nameClick()
    }
    
    static func getInstance()->UserHeaderView{
        return Bundle.main.loadNibNamed("UserHeaderView", owner: nil, options: nil)?.first as! UserHeaderView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewFrame = self.userImageView.frame
        // 设置头像圆角
        var bounds = self.userImageView.bounds
        bounds.size.height -= 4
        bounds.size.width -= 4
        bounds.origin.x += 1
        bounds.origin.y += 1
        // Add rounded corners
       let maskLayer = CAShapeLayer()
       maskLayer.frame = bounds//self.AutorImageView.bounds
        maskLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 30).cgPath
        self.userImageView.layer.mask = maskLayer
        
       // Add border
       let borderLayer = CAShapeLayer()
       borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
       borderLayer.strokeColor = UIColor.lightGray.cgColor
        borderLayer.lineWidth = 0.5
       borderLayer.frame = bounds
        self.userImageView.layer.addSublayer(borderLayer)
        
        if #available(iOS 13.0, *) {
            self.backgroundColor = Colors.backgroundColor
            self.userDescLabel.textColor = Colors.labelColor
           
        } else {
            self.backgroundColor = .white
            self.userDescLabel.textColor = .darkGray
        }
       
        resetUI()
    }
    
    func resetUI(){
    
       userImageView.image = UIImage(named: "image_loading")
       userNameLabel.text = "点击登录"
        joinDateLabel.text = "加入时间 "
        
        self.userNameLabel.textColor = Colors.BLUE_COLOR
        followsLabel.textColor = Colors.BLUE_COLOR
        staredLabel.textColor = Colors.BLUE_COLOR
        followingLabel.textColor = Colors.BLUE_COLOR
        watchedLabel.textColor = Colors.BLUE_COLOR
        followsLabel.text = "粉丝 \n 0"
        followingLabel.text = "关注 \n 0"
        staredLabel.text = "点赞 \n 0"
        watchedLabel.text = "观察 \n 0"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageViewFrame = bounds
    }
    func scrollViewDidScroll(contentOffsetY: CGFloat) {
        var frame = imageViewFrame!
        frame.size.height -= contentOffsetY
        frame.origin.y = contentOffsetY
        userImageView.frame = frame
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI(user: UserModel){
        
        let icon = URL(string: user.portrait_url ?? user.new_portrait!)
       userImageView.kf.setImage(with: icon, placeholder: UIImage(named: "image_loading"), options: .none, progressBlock: nil, completionHandler: nil)
       
        userNameLabel.text = user.name
        joinDateLabel.text = "加入时间 " + user.created_at.intervalSinceNow()
       
        if user.follow != nil {
            followsLabel.text = "粉丝 \n " + (user.follow["followers"]! as AnyObject).debugDescription
            followingLabel.text = "关注 \n " + (user.follow["following"]! as AnyObject).debugDescription
            staredLabel.text = "点赞 \n " + (user.follow["starred"]! as AnyObject).debugDescription
            watchedLabel.text = "观察 \n " + (user.follow["watched"]! as AnyObject).debugDescription
        }
    }

}
