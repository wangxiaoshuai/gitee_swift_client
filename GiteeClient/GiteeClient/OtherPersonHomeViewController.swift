//
//  OtherPersonHomeViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/29.
//

import UIKit
import  SnapKit
import JXSegmentedView


class OtherPersonHomeViewController: BasePageViewController {
   
    var model:UserModel?{
        didSet{
            self.userHeaderView.model = model
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
      }
    
    
    func loadData(){
        guard let user = model else {
            return
        }
        self.model = user
    }
    
    init(model:UserModel?,titles:[String], tableHeaderH: Int = 0) {
        super.init(titles: titles, tableHeaderH: tableHeaderH)
        self.model = model
        self.title = model?.name ?? ""
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // 设置项目列表控制器
    override func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        switch index {
        case 1:
            let list = ProjectListViewController(type: .PROJECT_OHTER, user: self.model)
            return list
        case 2:
            let list = ProjectListViewController(type: .STARED_PRO, user: self.model)
            return list
        case 3:
            let list = ProjectListViewController(type: .WATCH_PRO, user: self.model)
            return list
        default:
            let list = EventListViewController(user: self.model, type: .OTHER)
            return list
        }
    }
    
}
