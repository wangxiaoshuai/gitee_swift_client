//
//  ProjectNormalTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/26.
//

import UIKit

class ProjectNormalTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if #available(iOS 13.0, *) {
            self.contentView.backgroundColor = Colors.backgroundColor
            cellNameLabel.textColor = Colors.labelColor
        } else {
            self.contentView.backgroundColor = .white
            cellNameLabel.textColor = .black
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
