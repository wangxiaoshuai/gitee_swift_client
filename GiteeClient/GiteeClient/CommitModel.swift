//
//  CommitModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

import UIKit
import HandyJSON

/// 提交模型
struct CommitModel: HandyJSON {

    // id
    var id:String!
    // title
    var title:String!
    
    var message:String!
    // short_id
    var short_id:String!
    // author_name
    var author_name:String!
    // author_email
    var author_email:String!
    // author_portrait
    var author_portrait:String!
    // created_at
    var created_at:String!
    // created_at
    var committed_date:String!
    // parents
    var parents:[Int]!
    
    var parent_ids:[String]!
    // author
    var author:UserModel!
}
