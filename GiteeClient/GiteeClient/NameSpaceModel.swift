//
//  NameSpaceModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/15.
//

import UIKit
import HandyJSON

struct NameSpaceModel: HandyJSON {

    var id:String!
    var name:String!
    var path:String!
    var owner_id:String!
    var created_at:String!
    var updated_at:String!
    var description:String!
    var address:String!
    var email:String?
    var url:String?
    var location:String?
    var isPublic:String?
    var enterprise_id:Int!
    var level:Int!
    var from:String?
    var outsourced:Bool!
    var avatar:String?
    
    init() {}
    
    mutating func mapping(mapper: HelpingMapper) {
        
        mapper <<<
            self.isPublic <-- "public"
    }
}
