//
//  MeViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/14.
//

import UIKit
import  SnapKit
import JXSegmentedView
import PKHUD

class MeViewController: BasePageViewController,userNameClickDelegate {
   
    var model:UserModel?{
        didSet{
            self.userHeaderView.model = model
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userHeaderView.delegate = self
        loadData()
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.model = UserDefaultTools.getUser()
        updateUserInfo(userid: self.model!.id)
    }
    
    func updateUserInfo(userid:Int){
        
        let url = URLs.V3URL + "users/" + userid.description
        // 异步加载
        DispatchQueue.global().async {
        
            let result:Result<UserModel> = NetWorkTool.shared.getRequest(url: url, parameters: nil)
            
            DispatchQueue.main.async {
                if result.code == 0 {
                    self.model = result.data
                    UserDefaultTools.setUser(user: result.data)
                }else {
                    HUD.flash(.label(result.message), delay: 2.0)
                }
            }
        }
        
    }
    
    func loadData(){
        guard let user = model else {
            return
        }
        self.model = user
    }
    
    init(model:UserModel?,titles:[String], tableHeaderH: Int = 0) {
        super.init(titles: titles, tableHeaderH: tableHeaderH)
        self.model = model
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func nameClick() {
        
        if UserDefaultTools.getToken() == nil {
            let vc = LoginViewController()
            vc.title = "登陆"
            let loginVC = BaseNavigationController(rootViewController: vc)
            loginVC.modalPresentationStyle = .fullScreen
            if #available(iOS 13.0, *) {
                loginVC.isModalInPresentation = true
            }
            self.present(loginVC, animated: true, completion: nil)
        }else {
            logout()
        }
        
    }
    
    func logout(){
        let actionVc = UIAlertController(title: "提示", message: "退出当前账户?", preferredStyle: .alert)
        let yesBtn = UIAlertAction(title: "确定", style: .destructive) { (ac) in
            
            UserDefaultTools.resetDefault()
            self.tabBarController?.selectedIndex = 0
        }
        let noBtn = UIAlertAction(title: "取消", style: .default, handler: nil)
        actionVc.addAction(yesBtn)
        actionVc.addAction(noBtn)
        self.present(actionVc, animated: true, completion: nil)
    }
    
    // 设置项目列表控制器
    override func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        switch index {
        case 1:
            let list = ProjectListViewController(type: .PROJECT_OWNER, user: self.model)
            return list
        case 2:
            let list = ProjectListViewController(type: .STARED_PRO, user: self.model)
            return list
        case 3:
            let list = ProjectListViewController(type: .WATCH_PRO, user: self.model)
            return list
        default:
            let list = EventListViewController(user: self.model, type: .OWNER)
            return list
        }
    }
    
}

