//
//  File.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/22.
//

import UIKit

/// 用于判断文件是否为图片 代码等
class File: NSObject {
    
    // 代码文件结尾
    static let codeFileSuffixes = [
        "java", "confg", "ini", "xml", "json", "txt", "go",
        "php", "php3", "php4", "php5", "js", "css", "html",
        "properties", "c", "hpp", "h", "hh", "cpp", "cfg",
        "rb", "example", "gitignore", "project", "classpath",
        "mm", "md", "rst", "vm", "cl", "py", "pl", "haml",
        "erb", "scss", "bat", "coffee", "as", "sh", "m", "pas",
        "cs", "groovy", "scala", "sql", "bas", "xml", "vb",
        "xsl", "swift", "ftl", "yml", "ru", "jsp", "markdown",
        "cshap", "apsx", "sass", "less", "ftl", "haml", "log",
        "tx", "csproj", "sln", "clj", "scm", "xhml", "xaml",
        "lua", "pch","jsx",
        "sty","cls","thm","tex","bst","config",
        "plist","storyboard","gradle","pro","pbxproj",
        "xcscheme","proto", "wxss", "wxml", "vi",
        "ctl","ts","kt","vue"
        ]
    
    // 特殊文件名
    static let specialFileNames = [
    "LICENSE", "README", "readme", "makefile", "gemfile",
    "gemfile.*", "gemfile.lock", "TODO", "CHANGELOG",
    "Podfile", "Podfile.lock","gradlew"
    ]
    
    // 图片结尾
    static let imageSuffixes = [
                       "png", "jpg", "jpeg", "jpe", "bmp", "exif", "dxf",
                       "wbmp", "ico", "jpe", "gif", "pcx", "fpx", "ufo",
                       "tiff", "svg", "eps", "ai", "tga", "pcd", "hdri"
                       ]
    
    // 获取文件的格式
    static func getFileNameSuffix(fileName: String)->String{
        
        guard let name = fileName.split(separator: ".").last?.lowercased() else {
            return ""
        }
        return name
    }
    
    // 是否是代码
    static func isCodeFile(fileName: String)->Bool{
        
        let suffix = getFileNameSuffix(fileName: fileName)
        for temp in codeFileSuffixes {
            if suffix == temp{
                return true
            }
        }
        for temp in specialFileNames {
            if fileName == temp{
                return true
            }
        }
        
        return false
    }
    
    // 是否是图片
    static func isImageFile(fileName: String)->Bool{
        
        let suffix = getFileNameSuffix(fileName: fileName)
        for temp in imageSuffixes {
            if suffix == temp{
                return true
            }
        }
        
        return false
    }
    
    
    static func escapeHTML(html: String)-> String
    {
        var result = html
        result = result.replacingOccurrences(of: "&", with: "&amp;")
        result = result.replacingOccurrences(of: "<", with: "&lt;")
        result = result.replacingOccurrences(of: ">", with: "&gt;")
        result = result.replacingOccurrences(of: "\"", with: "&quot;")
        result = result.replacingOccurrences(of: "'", with: "&#39;")
        
        return result
    }
   
    
    static func isNUllOrEmpty(string: String?)->Bool{
            
        return string == nil ? true : string!.isEmpty ? true : false
    }
  
}
