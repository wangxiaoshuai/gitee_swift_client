//
//  ProjectItemTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/14.
//

import UIKit
import Kingfisher

class ProjectItemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var AutorImageView: UIImageView!
    
    @IBOutlet weak var ProjectNameLabel: UILabel!
    
    @IBOutlet weak var LastUpdateTimeLabel: UILabel!
    
    @IBOutlet weak var ProjectDescLabel: UILabel!
    
    @IBOutlet weak var LangueButton: UIButton!
    
    @IBOutlet weak var StarButton: UIButton!
    
    @IBOutlet weak var ForkButton: UIButton!
    
    @IBOutlet weak var WatchButton: UIButton!
    
    @IBOutlet weak var commImageView: UIImageView!
    
    @IBOutlet weak var contentBgView: UIView!
    var model:ProjectMdoel?{
        didSet{
            guard let m = model else {
                return
            }
            setupUI(model: m)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // 设置头像圆角
        var bounds = self.AutorImageView.bounds
        bounds.size.height -= 4
        bounds.size.width -= 4
        bounds.origin.x += 1
        bounds.origin.y += 1
        // Add rounded corners
       let maskLayer = CAShapeLayer()
       maskLayer.frame = bounds//self.AutorImageView.bounds
        maskLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 20).cgPath
        self.AutorImageView.layer.mask = maskLayer
        
       // Add border
       let borderLayer = CAShapeLayer()
       borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
       borderLayer.strokeColor = UIColor.lightGray.cgColor
        borderLayer.lineWidth = 0.5
       borderLayer.frame = bounds
        self.AutorImageView.layer.addSublayer(borderLayer)
        
        if #available(iOS 13.0, *) {
            self.contentBgView.backgroundColor = Colors.backgroundColor
            self.ProjectDescLabel.textColor = Colors.labelColor
           
        } else {
            self.contentBgView.backgroundColor = .white
            self.ProjectDescLabel.textColor = .darkGray
        }
        self.ProjectNameLabel.textColor = Colors.BLUE_COLOR
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupUI(model: ProjectMdoel) {
        
         let icon = URL(string: model.owner.portrait_url!)
        AutorImageView.kf.setImage(with: icon, placeholder: UIImage(named: "image_loading"), options: .none, progressBlock: nil, completionHandler: nil)
        
        ProjectNameLabel.text = model.owner.name + " /" + model.name
        if let last_push_at = model.last_push_at {
            
            LastUpdateTimeLabel.text = "最近更新  " + last_push_at.intervalSinceNow()
        }else {
            LastUpdateTimeLabel.text = "最近更新  " + model.created_at.intervalSinceNow() 
        }
       
        ProjectDescLabel.text = model.description
        ProjectDescLabel.sizeToFit()
        
        LangueButton.setTitle(model.language, for: .normal)
        ForkButton.setTitle(model.forks_count!.description, for: .normal)
        StarButton.setTitle(model.stars_count!.description, for: .normal)
        WatchButton.setTitle(model.watches_count!.description, for: .normal)
        commImageView.isHidden = !model.recomm
        
        
        
    }
    
}
