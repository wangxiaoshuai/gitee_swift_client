//
//  ProjectOwnerTableViewCell.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/20.
//

import UIKit
import Kingfisher

/// 项目详情页的拥有者cell
class ProjectOwnerTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ownerImageView: UIImageView!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    var model:ProjectMdoel?{
        didSet{
            guard let m = model else { return }
            
            if let icon = URL(string: m.owner.portrait_url!) {
                ownerImageView.kf.setImage(with: icon, placeholder: UIImage(named: "image_loading"), options: .none, progressBlock: nil, completionHandler: nil)
            }
            
            ownerNameLabel.text = m.owner.name
            if let last_push_at = m.last_push_at {
                
                lastUpdateLabel.text = "最近更新  " + last_push_at.intervalSinceNow()
            }else {
                lastUpdateLabel.text = "最近更新  " + m.created_at.intervalSinceNow()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        var bounds = self.ownerImageView.bounds
        bounds.size.height -= 4
        bounds.size.width -= 4
        bounds.origin.x += 1
        bounds.origin.y += 1
        // Add rounded corners
       let maskLayer = CAShapeLayer()
       maskLayer.frame = bounds//self.AutorImageView.bounds
        maskLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 20).cgPath
        self.ownerImageView.layer.mask = maskLayer
        
       // Add border
       let borderLayer = CAShapeLayer()
       borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
       borderLayer.strokeColor = UIColor.lightGray.cgColor
        borderLayer.lineWidth = 0.5
       borderLayer.frame = bounds
        self.ownerImageView.layer.addSublayer(borderLayer)
        
        if #available(iOS 13.0, *) {
            self.contentView.backgroundColor = Colors.backgroundColor
            
        } else {
            self.contentView.backgroundColor = .white
        }
        self.ownerNameLabel.textColor = Colors.BLUE_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
