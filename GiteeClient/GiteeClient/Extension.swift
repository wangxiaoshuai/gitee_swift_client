//
//  Extension.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/15.
//

import UIKit

extension String {
    
    /// 转换为 xx小时前  等格式日期
    func intervalSinceNow() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        let date = dateFormatter.date(from: self)!
        let unitFlags:NSInteger =  NSInteger(NSCalendar.Unit.year.rawValue|NSCalendar.Unit.month.rawValue|NSCalendar.Unit.day.rawValue|NSCalendar.Unit.hour.rawValue|NSCalendar.Unit.minute.rawValue)
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let compsPast:NSDateComponents = calendar!.components(NSCalendar.Unit(rawValue: NSCalendar.Unit.RawValue(unitFlags)), from: date) as NSDateComponents
        let compsNow:NSDateComponents = calendar!.components(NSCalendar.Unit(rawValue: UInt(unitFlags)), from: Date()) as NSDateComponents
        
        let year = compsNow.year - compsPast.year
        let months = compsNow.month - compsPast.month + year * 12
        let days = compsNow.day - compsPast.day + months * 30
        let hours = compsNow.hour - compsPast.hour + days * 24
        let minutes = compsNow.minute - compsPast.minute + hours * 60
        
        if (minutes < 1) {
            return "刚刚"
        } else if (minutes < 60) {
            return "\(minutes)分钟前"
        } else if (hours < 24) {
            return "\(hours)小时前"
        } else if (hours < 48 && days == 1) {
            return "昨天"
        } else if (days < 30) {
            return "\(days)天前"
        } else if (days < 60) {
            return "一个月前"
        } else if (months < 12) {
            return "\(months)个月前"
        } else {
           
            return self.dateString()
        }
 
    }
    
    /// 根据format来获取当前字符串表示的日期字符串
    func dateString(format: String = "yyyy-MM-dd'T'HH:mm:ssZZZZZ") -> String{
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = format
        
        let date = string2Date(self, dateFormat: format)
        
        return  date2String(date)
    }
    
    //日期 -> 字符串
    func date2String(_ date:Date, dateFormat:String = "yyyy-MM-dd HH:mm:ss") -> String {
        let formatter = DateFormatter()
//        formatter.locale = Locale.init(identifier: "en")
        formatter.dateFormat = dateFormat
//        formatter.dateStyle = .full
        let date = formatter.string(from: date)
        return date
    }
    
    //字符串 -> 日期
    func string2Date(_ string:String, dateFormat:String = "yyyy-MM-dd HH:mm:ss") -> Date {
        let formatter = DateFormatter()
//        formatter.locale = Locale.init(identifier: "en")
        formatter.dateFormat = dateFormat
//        formatter.dateStyle = .full
        let date = formatter.date(from: string)
        return date!
    }
    
   
}

extension UIImage {
    
    static func imageFromColor(color: UIColor, viewSize: CGSize = CGSize(width: 10, height: 10)) -> UIImage{
            let rect: CGRect = CGRect(x: 0, y: 0, width: viewSize.width, height: viewSize.height)
            
            UIGraphicsBeginImageContext(rect.size)
            
            let context: CGContext = UIGraphicsGetCurrentContext()!
            context.setFillColor(color.cgColor)
            context.fill(rect)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsGetCurrentContext()
            return image!
        }
}


extension UIColor {
    
    static func colorFromRGB(rgbValue: Int)->UIColor{
        
        return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0,
                       green: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0,
                       blue: ((CGFloat)(rgbValue & 0xFF))/255.0,
                       alpha: 1)
        
    }
}
