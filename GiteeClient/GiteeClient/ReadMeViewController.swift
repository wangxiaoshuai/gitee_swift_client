//
//  ReadMeViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/22.
//

import UIKit
import PKHUD
import MarkdownView
import SnapKit

/// readme 控制器用webview 来展示
class ReadMeViewController: BaseViewController {

    lazy var contentView = MarkdownView()
    var model:ReadMeModel?
    {
        didSet{
            guard let html = model?.content else {
                return
            }
            
            contentView.load(markdown: html)
        }
    }
    
    var projectID:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadData()
    }
    
    init(pid: Int) {
        super.init(nibName: nil, bundle: nil)
        self.projectID = pid
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI(){
        self.title = "ReadMe"
        contentView.backgroundColor = .white
        view.addSubview(contentView)
        var frame = view.bounds
        frame.size.height -= Common.topHeight
        contentView.frame = frame
        
    }
    
    func loadData() {
        
        if projectID == 0 {
            HUD.flash(.label("项目ID为空"))
            return
        }
        
        HUD.show(.progress)
        DispatchQueue.global().async {
            let url = URLs.DETIAL_URL + self.projectID.description + "/readme"//+ "?private_token=Eeu8RUV6SgCacx3U45UP"
            let result:Result<ReadMeModel> = NetWorkTool.shared.getRequest(url: url, parameters: nil)
            DispatchQueue.main.async {
                HUD.hide()
                if result.code == 0 {
                    self.model = result.data
                }else {
                    HUD.flash(.label(result.message), delay: 2.0)
                }
            }
        }
    }
}


