//
//  IssueDetialViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/26.
//

import UIKit

class IssueDetialViewController: BasePageViewController {

    var issmodel:IssueModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 设置创建issue 评论按钮
        let addBtn = UIButton(type: .contactAdd)
        addBtn.addTarget(self, action: #selector(createIssueComment), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addBtn)
    }
    
    
    /// 创建新的issue
    @objc func createIssueComment(){
        
        if loginCommand() {
            let newVc = NewIssueCommentViewController(issue: self.issmodel!)
//            newVc.delegate = self
            self.navigationController?.pushViewController(newVc, animated: true)
        }
    }
    
    func loginCommand()->Bool{
        
        if UserDefaultTools.getToken() == nil {
            let vc = LoginViewController()
            vc.title = "登陆"
            let loginVC = BaseNavigationController(rootViewController: vc)
            loginVC.modalPresentationStyle = .fullScreen
            if #available(iOS 13.0, *) {
                loginVC.isModalInPresentation = true
            }
            self.present(loginVC, animated: true, completion: nil)
            return false
        }else {
            return true
        }
    }

    
    
    init(model: IssueModel,titles:[String],tableHeaderH:Int = 0) {
        super.init(titles: titles, tableHeaderH: tableHeaderH)
        self.issmodel = model
        self.title = "#\(model.iid.description)"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func preferredPagingView() -> JXPagingView {
        JXPagingListRefreshView(delegate: self)
    }
    
    override func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        switch index {
        case 0:
            return IssueDetialContentViewController(model: self.issmodel!)
       
        default:
            return IssueDetialCommentViewController(issue: self.issmodel!)
        }
    }
}



