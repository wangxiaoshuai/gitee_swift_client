//
//  MainViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/29.
//

import UIKit

class MainViewController: BasePageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchButton()
        if #available(iOS 13.0, *) {
            segmentedView.backgroundColor = Colors.navigationColor
        } else {
            segmentedView.backgroundColor = .white
        }
    }
    
    func setupSearchButton(){
        
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(seearchButtonClick))
        self.navigationItem.rightBarButtonItem = searchBtn
    }

    @objc
    func seearchButtonClick(){
        
        let vc = SearchViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func preferredPagingView() -> JXPagingView {
        JXPagingListRefreshView(delegate: self)
    }
    
    override func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        switch index {
        case 0:
            return ProjectListViewController(type: .FEATURED)
        case 1:
            return ProjectListViewController(type: .POPULAR)
        default:
            return ProjectListViewController(type: .LASTUPDATE)
        }
    }

}
