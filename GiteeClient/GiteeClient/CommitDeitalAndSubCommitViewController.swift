//
//  CommitDeitalAndSubCommitViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/27.
//

import UIKit

class CommitDeitalAndSubCommitViewController: BasePageViewController {
    
    var commitmodel:CommitModel?
    var projectM:ProjectMdoel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 设置创建评论按钮
        let addBtn = UIButton(type: .contactAdd)
        addBtn.addTarget(self, action: #selector(createCommitComment), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addBtn)
    }
    
    
    /// 创建新的issue
    @objc func createCommitComment(){
        
        if loginCommand() {
            let newVc = NewCommitCommentViewController(commit: self.commitmodel!, project: self.projectM!)
            self.navigationController?.pushViewController(newVc, animated: true)
        }
    }
    
    func loginCommand()->Bool{
        
        if UserDefaultTools.getToken() == nil {
            let vc = LoginViewController()
            vc.title = "登陆"
            let loginVC = BaseNavigationController(rootViewController: vc)
            loginVC.modalPresentationStyle = .fullScreen
            if #available(iOS 13.0, *) {
                loginVC.isModalInPresentation = true
            }
            self.present(loginVC, animated: true, completion: nil)
            return false
        }else {
            return true
        }
    }
    
    init(model: CommitModel, project: ProjectMdoel,titles:[String],tableHeaderH:Int = 0) {
        super.init(titles: titles, tableHeaderH: tableHeaderH)
        self.commitmodel = model
        self.projectM = project
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    
    override func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        switch index {
        case 0:
            return CommitDetialViewController(project: self.projectM!, commitM: self.commitmodel!)
        default:
            return CommitDetialCommitsViewController(commit: self.commitmodel!, project: self.projectM!)
        }
    }

}
