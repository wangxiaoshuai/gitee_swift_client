//
//  NewIssueCommentViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/30.
//

import UIKit
import SnapKit
import PKHUD


protocol NewIssueCommentViewControllerDelegate {
    func didCreateIssueComment()
}

/// 评论问题控制器 可回复别人的issue
class NewIssueCommentViewController: BaseViewController {

    var delegate:NewIssueCommentViewControllerDelegate?
    
    var contentLabel:UILabel!
    var contentTextArea:UITextView!
    var doneButton:UIButton!

    var issueM:IssueModel?
    
    init(issue:IssueModel) {
        super.init(nibName: nil, bundle: nil)
        
        self.issueM = issue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
}

// MARK: - 设置界面
extension NewIssueCommentViewController {
    
    func setupUI() {
    
        if #available(iOS 13.0, *) {
            view.backgroundColor = Colors.backgroundColor
        }
        title = "发表评论"
        
        contentLabel = UILabel()
        contentLabel.textColor = Colors.BLUE_COLOR
        contentLabel.text = "评论内容"
        view.addSubview(contentLabel)
        
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        contentTextArea = UITextView()
        view.addSubview(contentTextArea)
        contentTextArea.font = .systemFont(ofSize: 16)
        contentTextArea.layer.borderWidth = 0.5
        contentTextArea.layer.borderColor = Colors.BLUE_COLOR.cgColor
        if #available(iOS 13.0, *) {
            contentTextArea.backgroundColor = Colors.backgroundColor
        }
        contentTextArea.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(contentLabel.snp.bottom).offset(5)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(200)
        }
        
        doneButton = UIButton(type: .custom)
        doneButton.setTitle("提交", for: .normal)
        doneButton.setTitleColor(.white, for: .normal)
        doneButton.backgroundColor = Colors.BLUE_COLOR
        view.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(doneButtonClick), for: .touchUpInside)
        doneButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(contentTextArea.snp.bottom).offset(30)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(44)
        }
        
    }
}

extension NewIssueCommentViewController {
    
    @objc func doneButtonClick(){
        
        if File.isNUllOrEmpty(string: contentTextArea.text) {
            HUD.flash(.label("评论内容不能为空"), delay: 1.0)
            return
        }
        
        HUD.show(.progress)
        var comment = CommentModel()
        comment.private_token = UserDefaultTools.getToken()!
        comment.body = contentTextArea.text
        
        let url = URLs.V3URL + URLs.PROJECTS + self.issueM!.project_id.description + "/issues/" + self.issueM!.id.description + "/notes"
        // 异步加载
        DispatchQueue.global().async {
        
            let result:Result<CommentModel> = NetWorkTool.shared.getRequest(url: url, method: .post, parameters: comment.toJSON())
            
            DispatchQueue.main.async {
                
                HUD.hide()
                if result.code == 0 {
                    guard let _ = result.data else {
                        HUD.flash(.label("创建失败"), delay: 1.0)
                        return
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.didCreateIssueComment()
                    
                }else {
                    HUD.flash(.label(result.message), delay: 2.0)
                }
            }
        }
    }
}
