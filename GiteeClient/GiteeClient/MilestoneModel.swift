//
//  MilestoneModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/23.
//

import UIKit
import HandyJSON

struct MilestoneModel: HandyJSON {

    // id
    var milestoneId:Int!
    // iid
    var milestoneIid:Int!
    // project_id
    var projectId:Int!
    // title
    var title:String!
    // description
    var milestoneDescription:String!
    // due_date
    var dueDate:String!//Date!
    // state
    var state:String!
    // updated_at
    var updatedAt:String!//Date!
    // created_at
    var createdAt:String!//Date!
}
