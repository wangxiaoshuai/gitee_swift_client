//
//  LanguageModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/28.
//

import UIKit
import HandyJSON

struct LanguageModel: HandyJSON {

    var id:Int!
    var name:String!
    var ident:String!
    var detial:String?
    var parent_id:Int!
    var order:Int!
    var created_at:String!
    var updated_at:String!
    var projects_count:Int!
    var codes_order:Int!
    var root_id:Int!
    var name_z:String!
    var name_en:String!
    
}
