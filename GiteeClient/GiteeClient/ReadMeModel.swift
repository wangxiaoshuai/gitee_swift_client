//
//  ReadMeModel.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/22.
//

import UIKit
import HandyJSON

struct ReadMeModel: HandyJSON {

    var content:String?
}
