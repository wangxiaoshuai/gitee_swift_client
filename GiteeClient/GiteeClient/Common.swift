//
//  Common.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/15.
//

import UIKit


/// 定义一些常用的全局常量
struct Common {

    /// 顶部高度等于 状体栏高度 + 导航栏高度
    static let statusHeight:CGFloat = UIApplication.shared.statusBarFrame.size.height
    static let topHeight:CGFloat = statusHeight + 44
    static let bottomHeight:CGFloat = statusHeight == 20 ? 49 : 83
    
    static let userToken = "usertoken"
    static let loginUser = "currentLoginUser"
}
