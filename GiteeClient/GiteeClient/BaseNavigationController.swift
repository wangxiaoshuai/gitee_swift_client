//
//  BaseNavigationController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/14.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.shadowImage = UIImage()
        if #available(iOS 13.0, *) {
            self.navigationBar.setBackgroundImage(UIImage.imageFromColor(color: Colors.navigationColor), for: .default)
        } else {
            // Fallback on earlier versions
            self.navigationBar.setBackgroundImage(UIImage.imageFromColor(color: .white), for: .default)
            
        }
        if #available(iOS 13.0, *) {
            self.navigationBar.tintColor = Colors.labelColor
        } else {
            self.navigationBar.tintColor = .black
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        if #available(iOS 13.0, *) {
            self.navigationBar.setBackgroundImage(UIImage.imageFromColor(color: Colors.navigationColor), for: .default)
            self.navigationBar.tintColor = Colors.labelColor
        } else {
            self.navigationBar.setBackgroundImage(UIImage.imageFromColor(color: .white), for: .default)
            self.navigationBar.tintColor = .black
            
        }
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
