//
//  IssueDetialContentViewController.swift
//  GiteeClient
//
//  Created by 王志标 on 2020/10/26.
//

import UIKit
import UITableView_FDTemplateLayoutCell

/// issue 详细内容控制器
class IssueDetialContentViewController: BaseViewController {

    var issueContenttableView:UITableView!
    var cellID = "issueContenttableView"
    
    var issueModel:IssueModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        issueContenttableView = UITableView(frame: view.bounds)
        view.addSubview(issueContenttableView)
        
        issueContenttableView.register(UINib.init(nibName: "IssueDetialTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        issueContenttableView.delegate = self
        issueContenttableView.dataSource = self
        issueContenttableView.separatorStyle = .none
        
        issueContenttableView.emptyDataSetDelegate = self
        issueContenttableView.emptyDataSetSource = self
        
        issueContenttableView.reloadData()
    }
    
    init(model: IssueModel) {
        super.init(nibName: nil, bundle: nil)
        self.issueModel = model
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension IssueDetialContentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! IssueDetialTableViewCell
        cell.model = self.issueModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        view.bounds.size.height
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension IssueDetialContentViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.issueContenttableView
    }
}
